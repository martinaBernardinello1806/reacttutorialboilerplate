/**
 * Gets the repositories of the user from Github
 */

import { call, put, select, takeLatest, all } from 'redux-saga/effects';
import { LOAD_REPOS } from 'containers/App/constants';
import { reposLoaded, repoLoadingError } from 'containers/App/actions';

import request from 'utils/request';
import { makeSelectUsername } from 'containers/HomePage/selectors';
import { API_CALL_REQUEST, API_PLACE_REQUEST } from './constants';

import {DogResponse, DogResponseError} from '../App/actions'

/**
 * Github repos request/response handler
 */
 function* getRepos() {
  // Select username from store
  const username = yield select(makeSelectUsername());
  const requestURL = `https://api.github.com/users/${username}/repos?type=all&sort=updated`;

  try {
    // Call our request helper (see 'utils/request')
    const repos = yield call(request, requestURL);
    yield put(reposLoaded(repos, username));
  } catch (err) {
    yield put(repoLoadingError(err));
  }
}

/**
 * Root saga manages watcher lifecycle
 */
export function* githubData() {
  // Watches for LOAD_REPOS actions and calls getRepos when one comes in.
  // By using `takeLatest` only the result of the latest API call is applied.
  // It returns task descriptor (just like fork) so we can continue execution
  // It will be cancelled automatically on component unmount
  yield takeLatest(LOAD_REPOS, getRepos);
}

function* getDog(){
  const dogURL = 'https://dog.ceo/api/breeds/image/random';

  try {
    // Call our request helper (see 'utils/request')
    const repos = yield call(request, dogURL);
    //console.log(repos);

    yield put(DogResponse(repos));
  } catch (err) {
    yield put(DogResponseError(err));
  }

}
 export function* dog(){
  yield takeLatest(API_CALL_REQUEST, getDog);
}

//immagini placeholder
function* getImagePlace(){
  const imageURL = 'https://jsonplaceholder.typicode.com/photos';
  try {
    const repos = yield call(request, imageURL);
    console.log(repos);
    yield put(DogResponse(repos));
  } catch (err) {
    yield put(DogResponseError(err));
  }
}
export function* imagePlace(){
  yield takeLatest(API_PLACE_REQUEST, getImagePlace);
}

//per esportare più funzioni
function* rootSaga () {
  yield all([
      dog(), // saga1 can also yield [ fork(actionOne), fork(actionTwo) ]
      githubData(),
  ]);
}

export default rootSaga;
