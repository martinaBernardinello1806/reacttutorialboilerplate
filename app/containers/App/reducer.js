/*
 * AppReducer
 *
 * The reducer takes care of our data. Using actions, we can change our
 * application state.
 * To add a new action, add it to the switch statement in the reducer function
 *
 * Example:
 * case YOUR_ACTION_CONSTANT:
 *   return state.set('yourStateVariable', true);
 */

import { fromJS } from 'immutable';

import { LOAD_REPOS_SUCCESS, LOAD_REPOS, LOAD_REPOS_ERROR } from './constants';
import { API_CALL_REQUEST, API_CALL_SUCCESS, API_CALL_FAILURE, API_PLACE_REQUEST, API_PLACE_SUCCESS, API_PLACE_FAILURE} from '../HomePage/constants'


// The initial state of the App
const initialState = fromJS({
  loading: false,
  error: false,
  dog: '',
  currentUser: false,
  userData: {
    repositories: false,
  },
});

function appReducer(state = initialState, action) {
  console.log(action)

  switch (action.type) {
    case LOAD_REPOS:
      return state
        .set('loading', true)
        .set('error', false)
        .setIn(['userData', 'repositories'], false);
    case LOAD_REPOS_SUCCESS:
      return state
        .setIn(['userData', 'repositories'], action.repos)
        .set('loading', false)
        .set('currentUser', action.username);
    case LOAD_REPOS_ERROR:
      return state.set('error', action.error).set('loading', false);
    case API_CALL_REQUEST:
      return state
        .set('loading', true)
        .set('error', false)
    case API_CALL_SUCCESS:
      return state
        .set('dog', action.dog)
        .set('loading', false)
        .set('error', false)
    case API_CALL_FAILURE:
      return state
        .set('error', action.error)
        .set('loading', false)
        case API_PLACE_REQUEST:
      return state
        .set('loading', true)
        .set('error', false)
    case API_PLACE_SUCCESS:
      debugger;
      return state
        .set('image', action)
        .set('loading', false)
        .set('error', false)
    case API_PLACE_FAILURE:
      return state
        .set('error', action.error)
        .set('loading', false)
    default:
      return state;
  }
}

export default appReducer;
