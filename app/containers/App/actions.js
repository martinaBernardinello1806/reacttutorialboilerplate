/*
 * App Actions
 *
 * Actions change things in your application
 * Since this boilerplate uses a uni-directional data flow, specifically redux,
 * we have these actions which are the only way your application interacts with
 * your application state. This guarantees that your state is up to date and nobody
 * messes it up weirdly somewhere.
 *
 * To add a new Action:
 * 1) Import your constant
 * 2) Add a function like this:
 *    export function yourAction(var) {
 *        return { type: YOUR_ACTION_CONSTANT, var: var }
 *    }
 */

import { LOAD_REPOS, LOAD_REPOS_SUCCESS, LOAD_REPOS_ERROR } from './constants';
import { API_CALL_FAILURE, API_CALL_REQUEST, API_CALL_SUCCESS, API_PLACE_REQUEST, API_PLACE_SUCCESS, API_PLACE_FAILURE} from '../HomePage/constants'

/**
 * Load the repositories, this action starts the request saga
 *
 * @return {object} An action object with a type of LOAD_REPOS
 */
export function loadRepos() {
  return {
    type: LOAD_REPOS,
  };
}

/**
 * Dispatched when the repositories are loaded by the request saga
 *
 * @param  {array} repos The repository data
 * @param  {string} username The current username
 *
 * @return {object}      An action object with a type of LOAD_REPOS_SUCCESS passing the repos
 */
export function reposLoaded(repos, username) {
  return {
    type: LOAD_REPOS_SUCCESS,
    repos,
    username,
  };
}

/**
 * Dispatched when loading the repositories fails
 *
 * @param  {object} error The error
 *
 * @return {object}       An action object with a type of LOAD_REPOS_ERROR passing the error
 */
export function repoLoadingError(error) {
  return {
    type: LOAD_REPOS_ERROR,
    error,
  };
}


export function loadRequestDog() {
  console.log('action + requestdog action')
  return {
    type: API_CALL_REQUEST
  };
}

export function DogResponse(repos) {
  console.log('actions Response')
  console.log(repos.message)
  return {
    type: API_CALL_SUCCESS,
    dog: repos.message  //repos.message
  };
}


export function DogResponseError(error) {
  console.log('error action')
  console.log(error)
  return {
    type: API_CALL_FAILURE,
    error
  };
}

///image placeholder
export function loadRequestPlace() {
  return {
    type: API_PLACE_REQUEST
  };
}

export function ImageResponse(repos) {
  console.log('actions Response')
  console.log(repos)
  return {
    type: API_PLACE_SUCCESS,
    repos
  };
}


export function ImageResponseError(error) {
  console.log('error action')
  console.log(error)
  return {
    type: API_PLACE_FAILURE,
    error
  };
}


